
# Usage

To install a single module, run:

    auropm i -f <module-path>


# Project

To create a project:
- Create a directory for your project
- Create a file configuration file `auropm.yml`
- Add a namespace for your project in the confiuration: `namespace: myproject`
- Create a directory `src/` for your source files
- Add a file `main.au`

Every file in the source directory will compile to a module with the name of the file without the extension and prefixed by the namespace.

The main module takes the name of the namespace. The file for the main module can be configured with the `main` key.

The `modules` configuration key can select the specific modules and set their compilation order. Later modules can see earlier modules.

Running `auropm i` in the project directory will compile and install all the modules in the project.



# Future Examples

None are implemented yet. I'm just using this as reference for future work.

Running scripts through auro and auropm:
```
$ auropm run example.lua
> Extension '.lua' not recognized.
> Consider installing the compiler with 'auropm install-compiler --extension .lua'.

$ auropm install-compiler --extension .lua
> Lua compiler installed.

$ auropm run example.lua
> Hello World!

$ auropm install example.lua
> Installed module 'example'

$ nugget example
> Hello World!
```

Building and installing packages:
```
$ ls
> auropm.json    test.au    main.au

$ cat test.au
> void sayhi (string name) { println("hi " + name); }

$ cat auropm.json
> {"namespace":["example", "auropm"]}

$ cat test.au
> import example.auropm.sayhi; void main () { sayhi("juan"); }

$ auropm read-config
> AuroPM config from auropm.json
> compilers: aulang
> namespace: example, auropm
> sourceFiles: main.au, test.au
> mainModule: main

$ auropm install
> Installing example.auropm from auropm.json

$ nugget run example.auropm.test
> hi juan
```

Using modules or packages hosted online:
```
$ curl "https://localhost/utils"
> # This is yaml. AuroPM knows by the content-type
> namespace: utils
> modules:
>   wotd: /utils/wotd

$ cat test.au
> import auropm.utils$`https://localhost/utils`; void main () { print(utils.wotd.getWord()); }

$ auropm run test.au
> spoon

$ cat test2.au
> import auropm.yolo$`https://localhost/utils/wotd`; void main () { print(yolo.getWord()); }
```

### Aulang compiler definition

```
// File: aulang.auropm
import auropm.compiler.{SourceFile, ItemCollector, Module};

type Parsed;
Parsed parse (SourceFile source, ItemCollector items);
Module compile (Parsed parsed, ModuleResolver resolver);

string[] extensions () { return string[]{".au"}; }
```

Installed with: `auropm install-compiler aulang.auropm`

### Alternative compiler definition

```
record Output {
    string name;
    buffer data;
}

Output[] compileFromFilenames (string[]);
```

# AuroPM as a nugget resolver

```c
#define NUGGET nuggetMachine nugget
nuggetModule auropmResolve (NUGGET, nuggetName name) {}

void main () {
    NUGGET = nuggetMachineCreate();
    nuggetAddNamespaceResolver("auropm", auropmResolve);
}
```

# Even better auropm resolver

```
// Module: auropm.nuggetResolver
type Resolver;
Resolver createResolver ();
nugget.Module resolve (Resolver);
```

```
# File: ~/config/nugget/.env
export NUGGET_RESOLVERS=$NUGGET_RESOLVERS:auropm.nuggetResolver
```