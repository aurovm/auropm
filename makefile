
.PHONY: install uninstall

install:
	install -m 755 auropm /usr/bin

uninstall:
	rm /usr/bin/auropm
